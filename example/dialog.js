const { waitProps, sendResponse } = require('electron-custom-dialog')

function renderTemplate(selector, template, options) {
  let html = template
  Object.keys(options).forEach(optKey => {
    html = html.replace(new RegExp(`\\$\\{${optKey}\\}`, 'g'), options[optKey])
  })
  var node = document.querySelector(selector)
  if (!node) return
  node.innerHTML = html
}
let answerGiven = false
waitProps().then(props => {
  const d = document.getElementById('dialogContent')
  const tmpl = d.innerHTML
  renderTemplate('#root', tmpl, props)

  var yesBtn = document.getElementById('yesBtn')
  yesBtn.addEventListener('click', () => {
    answerGiven = true
    sendResponse(true)
  })

  var noBtn = document.getElementById('noBtn')
  noBtn.addEventListener('click', () => {
    answerGiven = true
    sendResponse(false)
  })
})

window.onbeforeunload = e => {
  if (!answerGiven) {
    e.returnValue = false // equivalent to `return false` but not recommended
  }
}
