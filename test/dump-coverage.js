const fs = require('fs')
const path = require('path')
const electron = require('electron')
const COVERAGE_FOLDER = path.join(process.cwd(), '.nyc_output')

function dumpCoverage(cov) {
  if (cov) {
    fs.writeFileSync(
      path.resolve(COVERAGE_FOLDER, `${Date.now()}.json`),
      JSON.stringify(cov, null, 2),
      'utf8'
    )
  }
}

function dumpCoverageOnClose() {
  if (electron.remote) {
    window.addEventListener('beforeunload', e => dumpCoverage(window.__coverage__))
  } else {
    process.on('exit', () => dumpCoverage(global.__coverage__))
  }
}

module.exports = { dumpCoverage, dumpCoverageOnClose }
